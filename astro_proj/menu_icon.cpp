#include "menu_icon.h"

menu_icon::menu_icon(int x, int y, int width, int height, Color color)
{
	set_size(width, height);
	set_position(x, y);
	rect1.setFillColor(color);
	rect2.setFillColor(color);
	rect3.setFillColor(color);
}

void menu_icon::set_size(int _width, int _height)
{
	width = _width;
	height = _height;
	rect1.setSize(Vector2f(width, height / 5.0));
	rect2.setSize(Vector2f(width, height / 5.0));
	rect3.setSize(Vector2f(width, height / 5.0));
}

void menu_icon::set_position(int _x, int _y)
{
	x = _x;
	y = _y;
	rect1.setPosition(x, y);
	rect2.setPosition(x, y + height * 2 / 5);
	rect3.setPosition(x, y + height * 4 / 5);
}

void menu_icon::draw(RenderWindow & window)
{
	window.draw(rect1);
	window.draw(rect2);
	window.draw(rect3);
}

bool menu_icon::is_clicked(RenderWindow & window)
{
	int mouse_x = Mouse::getPosition(window).x;
	int mouse_y = Mouse::getPosition(window).y;
	return (x < mouse_x && mouse_x < x + width && y < mouse_y && mouse_y < y + height);
}
