#include "button.h"

button::button(string text_s, int x, int y, int width, int height, int _character_size, Color _background_color, Color _text_color)
{
	character_size = _character_size;
	set_text(text_s);
	set_color(_background_color, _text_color);
	set_size(width, height);
	set_position(x, y);
}

void button::set_text(string _text_s)
{
	text_s = _text_s;
}

void button::set_color(Color _background_color, Color _text_color)
{
	rect.setFillColor(_background_color);
	text_color = _text_color;
}

void button::set_size(int _width, int _height)
{
	width = _width;
	height = _height;
	rect.setSize(Vector2f(width, height));
}

void button::set_position(int _x, int _y)
{
	x = _x;
	y = _y;
	rect.setPosition(x, y);
}

void button::set_character_size(int _character_size)
{
	character_size = _character_size;
}

void button::draw(RenderWindow & window)
{
	Font font;
	Text text;

	font.loadFromFile("arial_narrow_7.ttf");
	text.setFont(font);
	text.setCharacterSize(character_size);
	text.setFillColor(text_color);
	//text.setFillColor(Color(255, 255, 255));
	text.setString(text_s);
	text.setPosition(x + 5, y);

	window.draw(rect);
	window.draw(text);
}

bool button::is_clicked(RenderWindow& window)
{
	int mouse_x = Mouse::getPosition(window).x;
	int mouse_y = Mouse::getPosition(window).y;
	return (x < mouse_x && mouse_x < x + width && y < mouse_y && mouse_y < y + height);
}
