#pragma once

#include "object.h"
#include <string>

using namespace std;
using namespace sf;

class task
{
public:
	object* first_obj, * second_obj;
	string config;
	task(object* first_obj = nullptr , object* second_obj = nullptr, string config = "");
};

