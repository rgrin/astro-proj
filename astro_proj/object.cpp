#include "object.h"

using namespace std;
using namespace sf;

object::object(object* center, string name, long double mass, long double object_radius, long double orbit_radius, long double real_orbit_radius, long double angle) : center(center), name(name), mass(mass), object_radius(object_radius), orbit_radius(orbit_radius), real_orbit_radius(real_orbit_radius)
{
	if (center != nullptr)
	{
		x_pos = orbit_radius * cos(angle * pi / 180);
		y_pos = -orbit_radius * sin(angle * pi / 180);
		real_x_pos = real_orbit_radius * cos(angle * pi / 180);
		real_y_pos = -real_orbit_radius * sin(angle * pi / 180);
		orbit_period = sqrt(2e30 / center->get_mass() * pow(real_orbit_radius, 3));
	}
	else
	{
		x_pos = 0;
		y_pos = 0;
		real_x_pos = 0;
		real_y_pos = 0;
		orbit_period = 0;
	}

	fixed = false;

	obj.setOrigin(3, 3);
	obj.setPosition(x_pos, y_pos);
	obj.setRadius(3);
	obj.setFillColor(Color(0, 255, 0));

	orbit.setFillColor(Color(0, 0, 0, 0));
	orbit.setOutlineThickness(2);
	orbit.setOutlineColor(Color(255, 255, 255, 50));
}

void object::set_orbit_radius(long double _orbit_radius)
{
	x_pos *= _orbit_radius / orbit_radius;
	y_pos *= _orbit_radius / orbit_radius;
	orbit_radius = _orbit_radius;
}

void object::set_angle(long double angle)
{
	x_pos = orbit_radius * cos(angle * pi / 180);
	y_pos = -orbit_radius * sin(angle * pi / 180);
}

void object::set_real_angle(long double angle)
{
	real_x_pos = real_orbit_radius * cos(angle * pi / 180);
	real_y_pos = -real_orbit_radius * sin(angle * pi / 180);
}

void object::set_center(object * new_center)
{
	center = new_center;
}

void object::set_in_position(object * another_planet, string type, bool new_connection)
{
	if (type == "Western square")
	{
		if (orbit_radius < another_planet->get_orbit_radius()) return;

		long double planet_obj_dist = sqrt(orbit_radius * orbit_radius - another_planet->get_orbit_radius() * another_planet->get_orbit_radius());
		long double planet_star_obj_angle = atan2(planet_obj_dist / orbit_radius, another_planet->get_orbit_radius() / orbit_radius);
		set_angle((another_planet->get_angle() + planet_star_obj_angle) * 180 / pi);

		long double real_planet_obj_dist = sqrt(real_orbit_radius * real_orbit_radius - another_planet->get_real_orbit_radius() * another_planet->get_real_orbit_radius());
		long double real_planet_star_obj_angle = atan2(real_planet_obj_dist / real_orbit_radius, another_planet->get_real_orbit_radius() / real_orbit_radius);
		set_real_angle((another_planet->get_real_angle() + real_planet_star_obj_angle) * 180 / pi);
	}
	else if (type == "Eastern square")
	{
		if (orbit_radius < another_planet->get_orbit_radius()) return;

		long double planet_obj_dist = sqrt(orbit_radius * orbit_radius - another_planet->get_orbit_radius() * another_planet->get_orbit_radius());
		long double planet_star_obj_angle = atan2(planet_obj_dist / orbit_radius, another_planet->get_orbit_radius() / orbit_radius);
		set_angle((another_planet->get_angle() - planet_star_obj_angle) * 180 / pi);

		long double real_planet_obj_dist = sqrt(real_orbit_radius * real_orbit_radius - another_planet->get_real_orbit_radius() * another_planet->get_real_orbit_radius());
		long double real_planet_star_obj_angle = atan2(real_planet_obj_dist / real_orbit_radius, another_planet->get_real_orbit_radius() / real_orbit_radius);
		set_real_angle((another_planet->get_real_angle() - real_planet_star_obj_angle) * 180 / pi);
	}
	else if (type == "Confrontation")
	{
		if (orbit_radius < another_planet->get_orbit_radius()) return;

		set_angle(another_planet->get_angle() * 180 / pi);
		set_real_angle(another_planet->get_real_angle() * 180 / pi);
	}
	else if (type == "Connection")
	{
		if (orbit_radius < another_planet->get_orbit_radius()) return;

		set_angle(another_planet->get_angle() * 180 / pi - 180);
		set_real_angle(another_planet->get_real_angle() * 180 / pi - 180);
	}
	else if (type == "Western elongation")
	{
		if (orbit_radius > another_planet->get_orbit_radius()) return;

		long double planet_obj_dist = sqrt(another_planet->get_orbit_radius() * another_planet->get_orbit_radius() - orbit_radius * orbit_radius);
		long double planet_star_obj_angle = atan2(planet_obj_dist / another_planet->get_orbit_radius(), orbit_radius / another_planet->get_orbit_radius());
		set_angle((another_planet->get_angle() + planet_star_obj_angle) * 180 / pi);

		long double real_planet_obj_dist = sqrt(another_planet->get_real_orbit_radius() * another_planet->get_real_orbit_radius() - real_orbit_radius * real_orbit_radius);
		long double real_planet_star_obj_angle = atan2(real_planet_obj_dist / another_planet->get_real_orbit_radius(), real_orbit_radius / another_planet->get_real_orbit_radius());
		set_real_angle((another_planet->get_real_angle() + real_planet_star_obj_angle) * 180 / pi);
	}
	else if (type == "Eastern elongation")
	{
		if (orbit_radius > another_planet->get_orbit_radius()) return;

		long double planet_obj_dist = sqrt(another_planet->get_orbit_radius() * another_planet->get_orbit_radius() - orbit_radius * orbit_radius);
		long double planet_star_obj_angle = atan2(planet_obj_dist / another_planet->get_orbit_radius(), orbit_radius / another_planet->get_orbit_radius());
		set_angle((another_planet->get_angle() - planet_star_obj_angle) * 180 / pi);

		long double real_planet_obj_dist = sqrt(another_planet->get_real_orbit_radius() * another_planet->get_real_orbit_radius() - real_orbit_radius * real_orbit_radius);
		long double real_planet_star_obj_angle = atan2(real_planet_obj_dist / another_planet->get_real_orbit_radius(), real_orbit_radius / another_planet->get_real_orbit_radius());
		set_real_angle((another_planet->get_real_angle() - real_planet_star_obj_angle) * 180 / pi);
	}
	else if (type == "Bottom connection")
	{
		if (orbit_radius > another_planet->get_orbit_radius()) return;

		set_angle(another_planet->get_angle() * 180 / pi);
		set_real_angle(another_planet->get_real_angle() * 180 / pi);
	}
	else if (type == "Upper connection")
	{
		if (orbit_radius > another_planet->get_orbit_radius()) return;

		set_angle(another_planet->get_angle() * 180 / pi - 180);
		set_real_angle(another_planet->get_real_angle() * 180 / pi - 180);
	}

	if (new_connection)
	{
		another_planet->make_connection(this, type);
		if (type == "Western square") make_connection(another_planet, "Eastern elongation");
		else if (type == "Eastern square") make_connection(another_planet, "Western elongation");
		else if (type == "Confrontation") make_connection(another_planet, "Bottom connection");
		else if (type == "Connection") make_connection(another_planet, "Upper connection");
		else if (type == "Western elongation") make_connection(another_planet, "Eastern square");
		else if (type == "Eastern elongation") make_connection(another_planet, "Western square");
		else if (type == "Bottom connection") make_connection(another_planet, "Confrontation");
		else if (type == "Upper connection") make_connection(another_planet, "Connection");
	}
	if (!fixed)
	{
		fixed = true;
		recount_connections();
	}
}

void object::make_connection(object * another_planet, string type)
{
	connections.emplace_back(another_planet, type);
}

void object::recount_connections()
{
	for (auto&[another_planet, type] : connections)
	{
		another_planet->set_in_position(this, type, false);
	}
}

void object::clear_connections()
{
	connections.clear();
}

long double object::get_x_pos()
{
	return x_pos;
}

long double object::get_y_pos()
{
	return y_pos;
}

long double object::get_real_x_pos()
{
	return real_x_pos;
}

long double object::get_real_y_pos()
{
	return real_y_pos;
}

long double object::get_orbit_radius()
{
	return orbit_radius;
}

long double object::get_real_orbit_radius()
{
	return real_orbit_radius;
}

long double object::get_mass()
{
	return mass;
}

long double object::get_object_radius()
{
	return object_radius;
}

long double object::get_angle()
{
	return atan2(-y_pos / orbit_radius, x_pos / orbit_radius);
}

long double object::get_real_angle()
{
	return atan2(-real_y_pos / real_orbit_radius, real_x_pos / real_orbit_radius);
}

long double object::get_speed()
{
	if (center != nullptr) return sqrt(G * center->get_mass() / real_orbit_radius * ae);
	return 0;
}

long double object::get_orbit_period()
{
	return orbit_period;
}

long double object::get_dist_to_star()
{
	if (center == nullptr) return 0;
	return sqrt(pow(center->get_x_pos() - x_pos * real_orbit_radius / orbit_radius, 2) +
				pow(center->get_y_pos() - y_pos * real_orbit_radius / orbit_radius, 2));
}

long double object::get_3_obj_angle(object * obj_1, object * obj_2)
{
	long double a = get_dist_to_obj(obj_1);
	long double b = get_dist_to_obj(obj_2);
	long double c = obj_1->get_dist_to_obj(obj_2);
	long double angle_cos = (a * a + b * b - c * c) / (2 * a * b);
	if (angle_cos - 1.0 >= EPS) return 0.0;
	if (angle_cos + 1.0 <= EPS) return 180.0;
	long double angle = acos(angle_cos);
	return angle * 180 / pi;
}

long double object::get_dist_to_obj(object * another_planet)
{
	return sqrt((real_x_pos - another_planet->get_real_x_pos()) * (real_x_pos - another_planet->get_real_x_pos()) + (real_y_pos - another_planet->get_real_y_pos()) * (real_y_pos - another_planet->get_real_y_pos()));
	/*return sqrt(pow(x_pos * real_orbit_radius / orbit_radius - another_planet->get_x_pos() * another_planet->get_real_orbit_radius() / another_planet->get_orbit_radius(), 2) +
				pow(y_pos * real_orbit_radius / orbit_radius - another_planet->get_y_pos() * another_planet->get_real_orbit_radius() / another_planet->get_orbit_radius(), 2));*/
}

long double object::get_angular_dimension(object * another_planet)
{
	return 206265 * another_planet->get_object_radius() / (get_dist_to_obj(another_planet) * ae / 1000);
}

string object::get_name()
{
	return name;
}

void object::count_next_pos(long double time)
{
	if (center != nullptr) set_angle((get_angle() + (2 * pi) * time / (orbit_period * 365 * 24 * 60 * 60)) * 180 / pi);
}

void object::draw(RenderWindow& window, Text& text_name, long double zoom_k, long double center_x_pos, long double center_y_pos, long double window_center_x, long double window_center_y)
{
	obj.setPosition((x_pos - center_x_pos) / zoom_k + window_center_x, (y_pos - center_y_pos) / zoom_k + window_center_y);
	text_name.setPosition((x_pos - center_x_pos) / zoom_k + window_center_x, (y_pos - center_y_pos) / zoom_k + window_center_y);

	if (center != nullptr)
	{
		orbit.setOrigin(orbit_radius / zoom_k, orbit_radius / zoom_k);
		orbit.setPosition((center->get_x_pos() - center_x_pos) / zoom_k + window_center_x, (center->get_y_pos() - center_y_pos) / zoom_k + window_center_y);
		orbit.setRadius(orbit_radius / zoom_k);
	}

	window.draw(obj);
	if (center != nullptr) window.draw(orbit);
	window.draw(text_name);
}

long double object::to_mouse_dist2(RenderWindow& window, long double zoom_k, long double center_x_pos, long double center_y_pos, long double window_center_x, long double window_center_y)
{
	return ((x_pos - center_x_pos) / zoom_k + window_center_x - Mouse::getPosition(window).x) * ((x_pos - center_x_pos) / zoom_k + window_center_x - Mouse::getPosition(window).x) + ((y_pos - center_y_pos) / zoom_k + window_center_y - Mouse::getPosition(window).y) * ((y_pos - center_y_pos) / zoom_k + window_center_y - Mouse::getPosition(window).y);
}
