#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <fstream>

#include "button.h"
#include "menu_icon.h"
#include "menu.h"
#include "object.h"
#include "task.h"

using namespace std;
using namespace sf;

const int window_width = 640 * 2;
const int window_height = 480 * 1.5;

RenderWindow window(VideoMode(window_width, window_height), "Configs");
Font font;

menu my_menu(150, 300, window_width, window_height);

long double zoom_k = 0.015;
//const long double ae = 150000000000;
const long double km = 1000;
const long double kps = 1000;

long double iter_time = 131.072;
Text timescale;
int timescale_color_intense = 255;
bool paused = true;

vector <map <string, int>> indexes;
vector <vector <object*>> sys;
vector <vector <Text>> sys_names;
int cur_sys = -1;
int real_sys = -1;
int cur_center_obj_ind = 0;
int cur_setting_obj_ind = 0;
int system_center_x, system_center_y, system_center_x_addition, system_center_y_addition;

vector <int> task_settings;
vector <task> tasks;
int cur_question = -1;
int cur_ans = 0;
int cur_div = 0;

void add_obj(object* center, string name, long double mass, long double object_radius, long double orbit_radius, long double real_orbit_radius, long double angle);
void count_system();
void reset_fixed_flags();
int get_pointed_obj_ind();
void generate_sys(int planet_cnt);
bool generate_questions(int question_cnt);
void check_mouse();
void draw();
int main();

void add_obj(object* center, string name, long double mass, long double object_radius, long double orbit_radius, long double real_orbit_radius, long double angle)
{
	sys[cur_sys].push_back(new object(center, name, mass, object_radius, orbit_radius, real_orbit_radius, angle));

	sys_names[cur_sys].emplace_back();
	sys_names[cur_sys].back().setFont(font);
	sys_names[cur_sys].back().setCharacterSize(15);
	sys_names[cur_sys].back().setFillColor(Color(255, 255, 255));
	sys_names[cur_sys].back().setString(sys[cur_sys].back()->get_name());

	indexes[cur_sys][name] = sys[cur_sys].size() - 1;
}

void count_system()
{
	for (auto& obj : sys[cur_sys])
	{
		obj->count_next_pos(iter_time);
	}
}

void reset_fixed_flags()
{
	for (int i = 0; i < sys[cur_sys].size(); ++i)
	{
		sys[cur_sys][i]->fixed = false;
	}
}

int get_pointed_obj_ind()
{
	system_center_x = (my_menu.get_cur_pos().first + my_menu.get_cur_pos().second) / 2;
	system_center_y = window_height / 2;

	if (sys.size() == 0 || sys[cur_sys].size() == 0) return -1;
	if (my_menu.get_cur_pos().first <= Mouse::getPosition(window).x && Mouse::getPosition(window).x <= my_menu.get_cur_pos().second && 0 <= Mouse::getPosition(window).y && Mouse::getPosition(window).y <= window_height)
	{
		long double min_dist2 = window_width * window_width + window_height * window_height;
		int cur_ind = 0;
		int pointed_obj_ind = -1;
		for (auto& obj : sys[cur_sys])
		{
			long double cur_dist2 = obj->to_mouse_dist2(window, zoom_k, sys[cur_sys][cur_center_obj_ind]->get_x_pos(), sys[cur_sys][cur_center_obj_ind]->get_y_pos(), system_center_x + system_center_x_addition, system_center_y + system_center_y_addition);
			if (cur_dist2 <= 400 && cur_dist2 < min_dist2)
			{
				min_dist2 = cur_dist2;
				pointed_obj_ind = cur_ind;
			}
			++cur_ind;
		}
		return pointed_obj_ind;
	}
	else return -1;
}

void generate_sys(int planet_cnt)
{
	sys.emplace_back();
	sys_names.emplace_back();
	indexes.emplace_back();
	cur_sys = sys.size() - 1;
	add_obj(nullptr, "Star", (rand() % 100) * 1e29, (rand() % 100) * 1e3, 0, 0, 0);
	long double cur_orbit_radius = 0;
	for (int cur_planet_num = 0; cur_planet_num < planet_cnt; ++cur_planet_num)
	{
		cur_orbit_radius += (rand() % 45 + 5) / 10.0;
		add_obj(sys[cur_sys][0], "Planet " + to_string(cur_planet_num + 1), (rand() % 10000) * 1e23, (rand() % 100) * 1e3, cur_planet_num + 1, cur_orbit_radius, 0);
	}


	vector <bool> used(planet_cnt, false);
	int first_planet = rand() % planet_cnt, second_planet;
	used[first_planet] = true;
	for (int task_num = 0; task_num < planet_cnt - 1; ++task_num)
	{
		second_planet = rand() % planet_cnt;
		while (used[second_planet])
		{
			second_planet = (second_planet + 1) % planet_cnt;
		}
		used[second_planet] = true;

		int config_type = rand() % 6;
		string config;
		if (sys[cur_sys][first_planet + 1]->get_dist_to_star() < sys[cur_sys][second_planet + 1]->get_dist_to_star())
		{
			if (0 <= config_type && config_type <= 1) config = "Western square";
			else if (2 <= config_type && config_type <= 3) config = "Eastern square";
			else if (4 <= config_type && config_type <= 4) config = "Confrontation";
			else if (5 <= config_type && config_type <= 5) config = "Connection";
		}
		else
		{
			if (0 <= config_type && config_type <= 1) config = "Western elongation";
			else if (2 <= config_type && config_type <= 3) config = "Eastern elongation";
			else if (4 <= config_type && config_type <= 4) config = "Upper connection";
			else if (5 <= config_type && config_type <= 5) config = "Bottom connection";
		}
		if (cur_sys == real_sys) tasks.emplace_back(sys[cur_sys][first_planet + 1], sys[cur_sys][second_planet + 1], config);

		reset_fixed_flags();
		sys[cur_sys][second_planet + 1]->set_in_position(sys[cur_sys][first_planet + 1], config);

		if (rand() % 2 == 0) first_planet = second_planet;
	}
}

bool generate_questions(int question_cnt)
{
	my_menu.clear_questions();
	my_menu.upd_questions("Which picture suits the task?");
	my_menu.upd_question_type(1);
	my_menu.upd_user_ans(0);
	my_menu.upd_correct_ans(real_sys + 1);

	fstream file;
	file.open("data.txt", ios::in);
	vector <int> total(4);
	vector <int> answered(4);
	for (int i = 0; i < total.size(); ++i)
	{
		file >> total[i];
	}
	for (int i = 0; i < answered.size(); ++i)
	{
		file >> answered[i];
	}
	file.close();
	vector <int> accuracy(4);
	vector <int> acc_sum(4);
	for (int i = 0; i < accuracy.size(); ++i)
	{
		accuracy[i] = 100 - answered[i] * 100 / (total[i] + 1);
		if (i == 0) acc_sum[i] = accuracy[i];
		else acc_sum[i] = acc_sum[i - 1] + accuracy[i];
	}
	vector <int> question_types = {0, 0, 0, 0};
	for (int question_num = 0; question_num < question_cnt; ++question_num)
	{
		int cur_question_type = rand() % acc_sum.back();
		if (cur_question_type < acc_sum[0]) ++question_types[0];
		else if (cur_question_type < acc_sum[1]) ++question_types[1];
		else if (cur_question_type < acc_sum[2]) ++question_types[2];
		else if (cur_question_type < acc_sum[3]) ++question_types[3];
	}

	if (question_types[0] != 0)
	{
		my_menu.upd_questions("\nCount distances (in ae) between:");
		my_menu.upd_question_type(0);
		my_menu.upd_user_ans(-1);
		my_menu.upd_correct_ans(-1);
		int first_planet, second_planet;
		for (int question_num = 0; question_num < question_types[0]; ++question_num)
		{
			int loop_cnt = 0;
			long double dist;
			do
			{
				first_planet = rand() % (sys[real_sys].size() - 1);
				second_planet = rand() % (sys[real_sys].size() - 2);
				if (second_planet >= first_planet) second_planet = (second_planet + 1) % (sys[real_sys].size() - 1);
				++first_planet;
				++second_planet;
				++loop_cnt;
				dist = sys[real_sys][first_planet]->get_dist_to_obj(sys[real_sys][second_planet]);
			} while (loop_cnt < 10 &&
					((fabs(dist - fabs(sys[real_sys][first_planet]->get_dist_to_star() + sys[real_sys][second_planet]->get_dist_to_star())) <= 0.00001) ||
					 (fabs(dist - fabs(sys[real_sys][first_planet]->get_dist_to_star() - sys[real_sys][second_planet]->get_dist_to_star())) <= 0.00001)));
			if (loop_cnt == 10) return false;
			my_menu.upd_questions("\tPlanet" + to_string(first_planet) + " and Planet" + to_string(second_planet));
			my_menu.upd_question_type(2);
			my_menu.upd_user_ans(0);
			my_menu.upd_correct_ans(sys[real_sys][first_planet]->get_dist_to_obj(sys[real_sys][second_planet]));
			cout << dist << "\n\n";
		}
	}
	if (question_types[1] != 0)
	{
		my_menu.upd_questions("\nCount angles (in �):");
		my_menu.upd_question_type(0);
		my_menu.upd_user_ans(-1);
		my_menu.upd_correct_ans(-1);
		int first_planet, second_planet, third_planet;
		for (int question_num = 0; question_num < question_types[1]; ++question_num)
		{
			int loop_cnt = 0;
			long double angle;
			do
			{
				first_planet = rand() % (sys[real_sys].size() - 1);
				second_planet = rand() % (sys[real_sys].size() - 2);
				third_planet = rand() % (sys[real_sys].size() - 3);
				if (second_planet >= first_planet) second_planet = (second_planet + 1) % (sys[real_sys].size() - 1);
				if (third_planet >= first_planet) third_planet = (third_planet + 1) % (sys[real_sys].size() - 1);
				if (third_planet >= second_planet) third_planet = (third_planet + 1) % (sys[real_sys].size() - 1);
				++first_planet;
				++second_planet;
				++third_planet;
				++loop_cnt;
				angle = sys[real_sys][second_planet]->get_3_obj_angle(sys[real_sys][first_planet], sys[real_sys][third_planet]);
			} while (loop_cnt < 10 && ((fabs(angle - 0.0) < 0.00001) || (fabs(angle - 90.0) < 0.00001) || (fabs(angle - 180.0) < 0.00001)));
			if (loop_cnt == 10) return false;
			my_menu.upd_questions("\tPlanet" + to_string(first_planet) + "-Planet" + to_string(second_planet) + "-Planet" + to_string(third_planet));
			my_menu.upd_question_type(3);
			my_menu.upd_user_ans(0);
			my_menu.upd_correct_ans(angle);
			cout << angle << "\n\n";
		}
	}
	if (question_types[2] != 0)
	{
		my_menu.upd_questions("\nCount angular dimensions (in \"):");
		my_menu.upd_question_type(0);
		my_menu.upd_user_ans(-1);
		my_menu.upd_correct_ans(-1);
		for (int question_num = 0; question_num < question_types[2]; ++question_num)
		{
			long double angular_dimension;
			int first_planet = rand() % (sys[real_sys].size() - 1);
			int second_planet = rand() % (sys[real_sys].size() - 2);
			if (second_planet >= first_planet) second_planet = (second_planet + 1) % (sys[real_sys].size() - 1);
			++first_planet;
			++second_planet;
			angular_dimension = sys[real_sys][first_planet]->get_angular_dimension(sys[real_sys][second_planet]);
			my_menu.upd_questions("\tOf Planet" + to_string(second_planet) + " from Planet" + to_string(first_planet));
			my_menu.upd_question_type(4);
			my_menu.upd_user_ans(0);
			my_menu.upd_correct_ans(angular_dimension);
			cout << angular_dimension << "\n\n";
		}
	}
	if (question_types[3] != 0)
	{
		my_menu.upd_questions("\nCount phases:");
		my_menu.upd_question_type(0);
		my_menu.upd_user_ans(-1);
		my_menu.upd_correct_ans(-1);
		for (int question_num = 0; question_num < question_types[3]; ++question_num)
		{
			long double phase;
			int first_planet = rand() % (sys[real_sys].size() - 1);
			int second_planet = rand() % (sys[real_sys].size() - 2);
			if (second_planet >= first_planet) second_planet = (second_planet + 1) % (sys[real_sys].size() - 1);
			++first_planet;
			++second_planet;
			phase = (1.0 + cos(sys[real_sys][second_planet]->get_3_obj_angle(sys[real_sys][0], sys[real_sys][first_planet]) * pi / 180.0)) / 2.0;
			my_menu.upd_questions("\tOf Planet" + to_string(second_planet) + " from Planet" + to_string(first_planet));
			my_menu.upd_question_type(5);
			my_menu.upd_user_ans(0);
			my_menu.upd_correct_ans(phase);
			cout << sys[real_sys][second_planet]->get_3_obj_angle(sys[real_sys][0], sys[real_sys][first_planet]) << " " << phase << "\n\n";
		}
	}

	return true;
}

void check_mouse()
{
	static bool is_left_mouse_butoon_clicked = false;
	static bool arrows_locked = false;
	int state = 0;
	static int choosing_obj_state = 0;
	static string config_type = "";
	if (Mouse::isButtonPressed(Mouse::Left))
	{
		if (!is_left_mouse_butoon_clicked)
		{
			state = my_menu.check_mouse(window);
			is_left_mouse_butoon_clicked = true;

			if (state == 40)
			{
				sys.emplace_back();
				sys_names.emplace_back();
				indexes.emplace_back();
				cur_sys = sys.size() - 1;
				add_obj(nullptr, "Sun", 2.0e30, 7e5, 0.000, 0.000, 0);
				add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Mercury", 3.3e23, 2439.7, 1, 0.387,  0);
				add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Venus",   4.9e24, 6051.8, 2, 0.723,  0);
				add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Earth",   6.0e24, 6378.1, 3, 1.000,  0);
				add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Mars",    6.4e23, 3396.2, 4, 1.524,  0);
				add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Jupiter", 1.9e27, 71492,  5, 5.204,  0);
				add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Saturn",  5.7e26, 60268,  6, 9.555,  0);
				add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Uranus",  8.7e25, 25559,  7, 19.229, 0);
				add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Neptune", 1.0e26, 24764,  8, 30.104, 0);
			}

			if (state == 0) add_obj(nullptr, "Sun", 2.0e30, 7e5, 0.000, 0.000, 0);
			else if (state == 10 && sys.size() != 0 && sys[cur_sys].size() != 0) add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Mercury", 3.3e23, 2439.7, 1, 0.387,  0);
			else if (state == 11 && sys.size() != 0 && sys[cur_sys].size() != 0) add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Venus",   4.9e24, 6051.8, 2, 0.723,  0);
			else if (state == 12 && sys.size() != 0 && sys[cur_sys].size() != 0) add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Earth",   6.0e24, 6378.1, 3, 1.000,  0);
			else if (state == 13 && sys.size() != 0 && sys[cur_sys].size() != 0) add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Mars",    6.4e23, 3396.2, 4, 1.524,  0);
			else if (state == 14 && sys.size() != 0 && sys[cur_sys].size() != 0) add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Jupiter", 1.9e27, 71492,  5, 5.204,  0);
			else if (state == 15 && sys.size() != 0 && sys[cur_sys].size() != 0) add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Saturn",  5.7e26, 60268,  6, 9.555,  0);
			else if (state == 16 && sys.size() != 0 && sys[cur_sys].size() != 0) add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Uranus",  8.7e25, 25559,  7, 19.229, 0);
			else if (state == 17 && sys.size() != 0 && sys[cur_sys].size() != 0) add_obj(sys[cur_sys][indexes[cur_sys]["Sun"]], "Neptune", 1.0e26, 24764,  8, 30.104, 0);
			else if (state == 200)
			{
				indexes.clear();
				for (auto& i : sys)
				{
					for (auto& j : i)
					{
						delete j;
					}
				}
				sys.resize(0);
				sys_names.resize(0);
				cur_sys = -1;
				real_sys = -1;
				cur_center_obj_ind = 0;
				cur_setting_obj_ind = 0;
				my_menu.reset_data();
				tasks.resize(0);
				my_menu.tasks = tasks;
				arrows_locked = false;
			}
			else if (state == 300)
			{
				for (auto& i : sys[cur_sys])
				{
					i->clear_connections();
				}
				paused = !paused;
			}
			else if (state == -2)
			{
				int pointed_obj_ind = get_pointed_obj_ind();
				if (pointed_obj_ind != -1)
				{
					if (choosing_obj_state == 0)
					{
						cur_setting_obj_ind = pointed_obj_ind;
						cur_center_obj_ind = pointed_obj_ind;
						system_center_x_addition = 0;
						system_center_y_addition = 0;
						if (real_sys == -1) my_menu.set_page(6);

						my_menu.focused_obj.resize(0);
						my_menu.focused_obj.push_back(sys[cur_sys][cur_setting_obj_ind]);
						my_menu.data_type = 0;
					}
					else if (choosing_obj_state == 1)
					{
						reset_fixed_flags();
						sys[cur_sys][cur_setting_obj_ind]->set_in_position(sys[cur_sys][pointed_obj_ind], config_type);
						my_menu.set_page(6);
						choosing_obj_state = 0;
					}
					else if (choosing_obj_state == 2)
					{
						my_menu.focused_obj.resize(0);
						my_menu.focused_obj.push_back(sys[cur_sys][pointed_obj_ind]);
						choosing_obj_state = 3;
					}
					else if (choosing_obj_state == 3)
					{
						my_menu.focused_obj.push_back(sys[cur_sys][pointed_obj_ind]);
						my_menu.data_type = 1;
						choosing_obj_state = 0;
					}
					else if (choosing_obj_state == 4)
					{
						my_menu.focused_obj.resize(0);
						my_menu.focused_obj.push_back(sys[cur_sys][pointed_obj_ind]);
						choosing_obj_state = 5;
					}
					else if (choosing_obj_state == 5)
					{
						my_menu.focused_obj.push_back(sys[cur_sys][pointed_obj_ind]);
						choosing_obj_state = 6;
					}
					else if (choosing_obj_state == 6)
					{
						my_menu.focused_obj.push_back(sys[cur_sys][pointed_obj_ind]);
						my_menu.data_type = 2;
						choosing_obj_state = 0;
					}
				}
				else
				{
					cur_setting_obj_ind = 0;
					cur_center_obj_ind = 0;
					system_center_x_addition = 0;
					system_center_y_addition = 0;

					my_menu.focused_obj.resize(0);
					my_menu.data_type = -1;
				}
			}
			else if (state == 21)
			{
				long double new_angle;
				cout << "\t- New angle is ";
				cin >> new_angle;
				reset_fixed_flags();
				sys[cur_sys][cur_setting_obj_ind]->set_angle(new_angle);
				sys[cur_sys][cur_setting_obj_ind]->recount_connections();
			}
			else if (30 <= state && state <= 37)
			{
				choosing_obj_state = 1;

				if (state == 30) config_type = "Western square";
				else if (state == 31) config_type = "Eastern square";
				else if (state == 32) config_type = "Confrontation";
				else if (state == 33) config_type = "Connection";
				else if (state == 34) config_type = "Western elongation";
				else if (state == 35) config_type = "Eastern elongation";
				else if (state == 36) config_type = "Upper connection";
				else if (state == 37) config_type = "Bottom connection";
			}
			else if (state == 104)
			{
				choosing_obj_state = 0;
			}
			else if (state == 50)
			{
				choosing_obj_state = 2;
			}
			else if (state == 51)
			{
				choosing_obj_state = 4;
			}
			else if (state == 60)
			{
				do
				{
					indexes.clear();
					sys.resize(0);
					sys_names.resize(0);
					cur_center_obj_ind = 0;
					cur_setting_obj_ind = 0;
					my_menu.reset_data();
					tasks.resize(0);

					task_settings = my_menu.get_settings();
					real_sys = rand() % task_settings[2];
					cout << real_sys + 1 << "\n";
					for (int sys_num = 0; sys_num < task_settings[2]; ++sys_num)
					{
						generate_sys(task_settings[0]);
					}
					cur_sys = 0;

					my_menu.tasks = tasks;
				} while (!generate_questions(task_settings[1]));
			}
			else if (70 <= state && state <= 90)
			{
				cur_question = state - 70;
				cur_ans = 0;
				cur_div = 0;
			}
		}
	}
	else is_left_mouse_butoon_clicked = false;

	static bool is_right_mouse_button_clicked = false;
	if (Mouse::isButtonPressed(Mouse::Right))
	{
		static int prev_mouse_x_pos = 0;
		static int prev_mouse_y_pos = 0;
		if (!is_right_mouse_button_clicked)
		{
			is_right_mouse_button_clicked = true;
			prev_mouse_x_pos = Mouse::getPosition(window).x;
			prev_mouse_y_pos = Mouse::getPosition(window).y;
		}
		system_center_x_addition += Mouse::getPosition(window).x - prev_mouse_x_pos;
		system_center_y_addition += Mouse::getPosition(window).y - prev_mouse_y_pos;
		prev_mouse_x_pos = Mouse::getPosition(window).x;
		prev_mouse_y_pos = Mouse::getPosition(window).y;
	}
	else is_right_mouse_button_clicked = false;

	static bool is_a_key_pressed = false;
	if (Keyboard::isKeyPressed(Keyboard::A))
	{
		if (!is_a_key_pressed)
		{
			is_a_key_pressed = true;
			zoom_k /= 1.1;
		}
	}
	else is_a_key_pressed = false;

	static bool is_s_key_pressed = false;
	if (Keyboard::isKeyPressed(Keyboard::S))
	{
		if (!is_s_key_pressed)
		{
			is_s_key_pressed = true;
			zoom_k *= 1.1;
		}
	}
	else is_s_key_pressed = false;

	static bool is_z_key_pressed = false;
	if (Keyboard::isKeyPressed(Keyboard::Z))
	{
		if (!is_z_key_pressed)
		{
			is_z_key_pressed = true;
			iter_time = iter_time * 2;
			timescale_color_intense = 255;
		}
	}
	else is_z_key_pressed = false;

	static bool is_x_key_pressed = false;
	if (Keyboard::isKeyPressed(Keyboard::X))
	{
		if (!is_x_key_pressed)
		{
			is_x_key_pressed = true;
			iter_time = max((long double)131.072, iter_time / 2);
			timescale_color_intense = 255;
		}
	}
	else is_x_key_pressed = false;

	static bool is_left_key_pressed = false;
	if (Keyboard::isKeyPressed(Keyboard::Left) && !arrows_locked)
	{
		if (!is_left_key_pressed)
		{
			is_left_key_pressed = true;
			cur_sys = max(0, cur_sys - 1);
		}
	}
	else is_left_key_pressed = false;

	static bool is_right_key_pressed = false;
	if (Keyboard::isKeyPressed(Keyboard::Right) && !arrows_locked)
	{
		if (!is_right_key_pressed)
		{
			is_right_key_pressed = true;
			cur_sys = min((int)sys.size() - 1, cur_sys + 1);
		}
	}
	else is_right_key_pressed = false;

	if (cur_question != -1)
	{
		static bool is_0_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num0))
		{
			if (!is_0_key_pressed)
			{
				is_0_key_pressed = true;
				cur_ans = cur_ans * 10 + 0;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_0_key_pressed = false;

		static bool is_1_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num1))
		{
			if (!is_1_key_pressed)
			{
				is_1_key_pressed = true;
				cur_ans = cur_ans * 10 + 1;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_1_key_pressed = false;

		static bool is_2_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num2))
		{
			if (!is_2_key_pressed)
			{
				is_2_key_pressed = true;
				cur_ans = cur_ans * 10 + 2;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_2_key_pressed = false;

		static bool is_3_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num3))
		{
			if (!is_3_key_pressed)
			{
				is_3_key_pressed = true;
				cur_ans = cur_ans * 10 + 3;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_3_key_pressed = false;

		static bool is_4_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num4))
		{
			if (!is_4_key_pressed)
			{
				is_4_key_pressed = true;
				cur_ans = cur_ans * 10 + 4;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_4_key_pressed = false;

		static bool is_5_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num5))
		{
			if (!is_5_key_pressed)
			{
				is_5_key_pressed = true;
				cur_ans = cur_ans * 10 + 5;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_5_key_pressed = false;

		static bool is_6_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num6))
		{
			if (!is_6_key_pressed)
			{
				is_6_key_pressed = true;
				cur_ans = cur_ans * 10 + 6;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_6_key_pressed = false;

		static bool is_7_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num7))
		{
			if (!is_7_key_pressed)
			{
				is_7_key_pressed = true;
				cur_ans = cur_ans * 10 + 7;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_7_key_pressed = false;

		static bool is_8_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num8))
		{
			if (!is_8_key_pressed)
			{
				is_8_key_pressed = true;
				cur_ans = cur_ans * 10 + 8;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_8_key_pressed = false;

		static bool is_9_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Num9))
		{
			if (!is_9_key_pressed)
			{
				is_9_key_pressed = true;
				cur_ans = cur_ans * 10 + 9;
				cur_div *= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_9_key_pressed = false;

		static bool is_backspace_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::BackSpace))
		{
			if (!is_backspace_key_pressed)
			{
				is_backspace_key_pressed = true;
				cur_ans /= 10;
				cur_div /= 10;
				if (cur_div == 0) my_menu.upd_user_ans(cur_ans, cur_question);
				else my_menu.upd_user_ans((long double)cur_ans / cur_div, cur_question);
			}
		}
		else is_backspace_key_pressed = false;

		static bool is_period_key_pressed = false;
		if (Keyboard::isKeyPressed(Keyboard::Period))
		{
			if (!is_period_key_pressed)
			{
				is_period_key_pressed = true;
				if (cur_div == 0) cur_div = 1;
			}
		}
		else is_period_key_pressed = false;
	}

	
	static bool is_enter_pressed = false;
	if (Keyboard::isKeyPressed(Keyboard::Enter))
	{
		if (!is_enter_pressed)
		{
			is_enter_pressed = true;
			cur_question = -1;
			cur_ans = 0;
			cur_div = 0;
		}
	}
	else is_enter_pressed = false;
}

void draw()
{
	if (sys.size() != 0)
	{
		system_center_x = (my_menu.get_cur_pos().first + my_menu.get_cur_pos().second) / 2;
		system_center_y = window_height / 2;

		for (int i = 0; i < sys[cur_sys].size(); ++i)
		{
			sys[cur_sys][i]->draw(window, sys_names[cur_sys][i], zoom_k, sys[cur_sys][cur_center_obj_ind]->get_x_pos(), sys[cur_sys][cur_center_obj_ind]->get_y_pos(), system_center_x + system_center_x_addition, system_center_y + system_center_y_addition);
		}
	}

	my_menu.draw(window);

	/*timescale_color_intense = max(0, timescale_color_intense - 1);
	timescale.setFillColor(Color(255, 255, 255, timescale_color_intense));
	timescale.setString(to_string((int)(iter_time / 0.001)));
	timescale.setPosition(my_menu.get_cur_pos().second - 100, window_height - 25);
	window.draw(timescale);*/

	Text sys_page;
	sys_page.setFont(font);
	sys_page.setCharacterSize(15);
	sys_page.setFillColor(Color(255, 255, 255));
	sys_page.setString(to_string(cur_sys + 1) + " / " + to_string(sys.size()));
	sys_page.setPosition(my_menu.get_cur_pos().first + 10, window_height - 20);
	window.draw(sys_page);
}

int main()
{
	srand(time(NULL));
	font.loadFromFile("arial_narrow_7.ttf");

	Event event;
	Clock timer;

	

	font.loadFromFile("arial_narrow_7.ttf");
	timescale.setFont(font);
	timescale.setCharacterSize(20);
	timescale_color_intense = 255;

	system_center_x = window_width / 2;
	system_center_y = window_height / 2;
	system_center_x_addition = 0;
	system_center_y_addition = 0;

	while (window.isOpen())
	{
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
			{
				window.close();
			}
		}

		
		if (!paused) count_system();
		check_mouse();
		
		if (timer.getElapsedTime() >= milliseconds(1))
		{
			window.clear();
			draw();
			window.display();
			timer.restart();
		}
	}
	return 0;
}