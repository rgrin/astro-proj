#pragma once

#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include <fstream>

#include "menu_icon.h"
#include "button.h"
#include "object.h"
#include "task.h"

using namespace sf;
using namespace std;

class menu
{
private:
	const long double ae = 150000000000;
	const long double km = 1000;

	int window_width, window_height;

	bool is_closed;
	int left_width, right_width;
	int cur_left_pos, cur_right_pos, v;
	int page;

	string data;
	vector <vector <object*>> distances;
	vector <vector <object*>> angles;

	menu_icon icon;

	button practice_mode_button;
	button sandbox_button;
	int init_page;

	button add_system_button;
	button add_star_button;
	button add_planet_button;
	button count_smth_button;
	button pause_button;
	button clear_button;
	button sandbox_page_back_button;
	int sandbox_page;

	button add_solar_system_button;
	button add_system_page_back_button;
	int add_system_page;

	button add_sun_button;
	button add_star_page_back_button;
	int add_star_page;

	button add_mercury_button;
	button add_venus_button;
	button add_earth_button;
	button add_mars_button;
	button add_jupiter_button;
	button add_saturn_button;
	button add_uranus_button;
	button add_neptune_button;
	button add_planet_page_back_button;
	int add_planet_page;

	button count_dist_button;
	button count_angle_button;
	button count_page_back_button;
	int count_page;

	button set_angle_button;
	button add_config_button;
	button obj_settings_page_back_button;
	int obj_settings_page;

	button wq_button;
	button eq_button;
	button cs_button;
	button cn_button;
	button we_button;
	button ee_button;
	button dc_button;
	button uc_button;
	button config_page_back_button;
	int config_page;

	button easy_mode_button;
	button normal_mode_button;
	button hard_mode_button;
	button statistics_button;
	button practice_mode_page_back_button;
	int practice_mode_page;
	vector <int> settings;

	button next_button;
	button submit_button;
	button task_page_back_button;
	int task_page;
	vector <string> questions;
	vector <int> question_type;
	vector <long double> user_ans;
	vector <long double> correct_ans;
	bool submitted;

	button statistics_page_back_button;
	int statistics_page;

	RectangleShape background;
	RectangleShape data_background;
public:
	vector <object*> focused_obj;
	vector <task> tasks;
	int data_type;

	menu(int left_width, int right_width, int window_width, int window_height);
	int check_mouse(RenderWindow& window);
	void set_page(int new_page);
	pair <int, int> get_cur_pos();
	void move();
	void draw(RenderWindow& window);
	vector <int> get_settings();
	void reset_data();
	void upd_questions(string _questions);
	void upd_question_type(int _question_type);
	void upd_user_ans(long double _user_ans, int ind = -1);
	void upd_correct_ans(long double _correct_ans);
	void clear_questions();
};