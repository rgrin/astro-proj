#pragma once

#include <SFML/Graphics.hpp>
#include <string>

using namespace sf;
using namespace std;

class button
{
private:
	int x, y, width, height, character_size;
	RectangleShape rect;
	Color text_color;
	string text_s;
public:
	button(string text_s = "", int x = 0, int y = 0, int width = 0, int height = 0, int _character_size = 20, Color _background_color = Color(255, 255, 255), Color _text_color = Color(0, 0, 0));
	void set_text(string _text_s);
	void set_color(Color _background_color, Color _text_color);
	void set_size(int _width, int _height);
	void set_position(int _x, int _y);
	void set_character_size(int _character_size);
	void draw(RenderWindow& window);
	bool is_clicked(RenderWindow& window);
};

