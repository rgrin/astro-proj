#pragma once

#include <iostream>
#include <vector>
#include <cmath>
#include <string>
#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;

const long double G = 6.67e-11;
const long double pi = 3.14159265;
const long double ae = 150000000000;
const long double EPS = 0.0000000000000001;

class object
{
private:
	object* center;
	CircleShape obj, orbit;
	string name;
	long double x_pos, y_pos, real_x_pos, real_y_pos, mass, object_radius, real_orbit_radius, orbit_radius, orbit_period;
	long double radius;
public:
	bool fixed;
	vector <pair <object*, string>> connections;

	object(object* center = nullptr, string name = "", long double mass = 0, long double object_radius = 0, long double orbit_radius = 0, long double real_orbit_radius = 0, long double angle = 0);
	void set_orbit_radius(long double orbit_radius = 0);
	void set_angle(long double angle);
	void set_real_angle(long double angle);
	void set_center(object* new_center);
	void set_in_position(object* another_planet, string type, bool new_connection = true);
	void make_connection(object* another_planet, string type);
	void recount_connections();
	void clear_connections();
	long double get_x_pos();
	long double get_y_pos();
	long double get_real_x_pos();
	long double get_real_y_pos();
	long double get_orbit_radius();
	long double get_real_orbit_radius();
	long double get_mass();
	long double get_object_radius();
	long double get_angle();
	long double get_real_angle();
	long double get_speed();
	long double get_orbit_period();
	long double get_dist_to_star();
	long double get_3_obj_angle(object* obj_1, object* obj_2);
	long double get_dist_to_obj(object* another_planet);
	long double get_angular_dimension(object* another_planet);
	string get_name();
	void count_next_pos(long double time);
	void draw(RenderWindow& window, Text& text_name, long double zoom_k, long double center_x_pos, long double center_y_pos, long double window_center_x, long double window_center_y);

	long double to_mouse_dist2(RenderWindow& window, long double zoom_k, long double center_x_pos, long double center_y_pos, long double window_center_x, long double window_center_y);
};