#pragma once

#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;

class menu_icon
{
private:
	int x, y, width, height;
	RectangleShape rect1, rect2, rect3;
public:
	menu_icon(int x = 0, int y = 0, int width = 0, int height = 0, Color color = Color(255, 255, 255));
	void set_size(int _width, int _height);
	void set_position(int _x, int _y);
	void draw(RenderWindow& window);
	bool is_clicked(RenderWindow& window);
};

