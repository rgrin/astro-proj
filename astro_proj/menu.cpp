#include "menu.h"

menu::menu(int left_width, int right_width, int window_width, int window_height) : left_width(left_width), right_width(right_width), window_width(window_width), window_height(window_height)
{
	focused_obj.resize(0);
	data_type = -1;

	is_closed = false;
	cur_left_pos = 0;
	cur_right_pos = window_width;
	v = 10;

	settings = {0, 0, 0};

	submitted = false;


	init_page = 0;
	sandbox_page = 1;
	add_system_page = 2;
	add_star_page = 3;
	add_planet_page = 4;
	count_page = 5;
	obj_settings_page = 6;
	config_page = 7;
	practice_mode_page = 8;
	task_page = 9;
	statistics_page = 10;
	page = practice_mode_page;

	data = "";
	distances.resize(0);

	//thare are 7 pages now
	icon.set_size(20, 20);
	icon.set_position(0, 0);



	practice_mode_button.set_text("Practice mode");
	practice_mode_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	practice_mode_button.set_size(left_width - 20, 30);
	practice_mode_button.set_position(-left_width + 10, 10 - init_page * window_height);

	sandbox_button.set_text("Sandbox mode");
	sandbox_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	sandbox_button.set_size(left_width - 20, 30);
	sandbox_button.set_position(-left_width + 10, 50 - init_page * window_height);



	add_system_button.set_text("Add system");
	add_system_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_system_button.set_size(left_width - 20, 30);
	add_system_button.set_position(-left_width + 10, 10 - sandbox_page * window_height);

	add_star_button.set_text("Add star");
	add_star_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_star_button.set_size(left_width - 20, 30);
	add_star_button.set_position(-left_width + 10, 50 - sandbox_page * window_height);

	add_planet_button.set_text("Add planet");
	add_planet_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_planet_button.set_size(left_width - 20, 30);
	add_planet_button.set_position(-left_width + 10, 90 - sandbox_page * window_height);

	count_smth_button.set_text("Counting");
	count_smth_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	count_smth_button.set_size(left_width - 20, 30);
	count_smth_button.set_position(-left_width + 10, 130 - sandbox_page * window_height);

	pause_button.set_text("Start/Pause");
	pause_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	pause_button.set_size(left_width - 20, 30);
	pause_button.set_position(-left_width + 10, 170 - sandbox_page * window_height);

	clear_button.set_text("Clear system");
	clear_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	clear_button.set_size(left_width - 20, 30);
	clear_button.set_position(-left_width + 10, 210 - sandbox_page * window_height);

	sandbox_page_back_button.set_text("Back");
	sandbox_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	sandbox_page_back_button.set_size(left_width - 20, 30);
	sandbox_page_back_button.set_position(-left_width + 10, 440 - sandbox_page * window_height);



	add_solar_system_button.set_text("Solar system");
	add_solar_system_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_solar_system_button.set_size(left_width - 20, 30);
	add_solar_system_button.set_position(-left_width + 10, 10 - add_system_page * window_height);

	add_system_page_back_button.set_text("Back");
	add_system_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_system_page_back_button.set_size(left_width - 20, 30);
	add_system_page_back_button.set_position(-left_width + 10, 440 - add_system_page * window_height);



	add_sun_button.set_text("Sun");
	add_sun_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_sun_button.set_size(left_width - 20, 30);
	add_sun_button.set_position(-left_width + 10, 10 - add_star_page * window_height);

	add_star_page_back_button.set_text("Back");
	add_star_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_star_page_back_button.set_size(left_width - 20, 30);
	add_star_page_back_button.set_position(-left_width + 10, 440 - add_star_page * window_height);



	add_mercury_button.set_text("Mercury");
	add_mercury_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_mercury_button.set_size(left_width - 20, 30);
	add_mercury_button.set_position(-left_width + 10, 10 - add_planet_page * window_height);

	add_venus_button.set_text("Venus");
	add_venus_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_venus_button.set_size(left_width - 20, 30);
	add_venus_button.set_position(-left_width + 10, 50 - add_planet_page * window_height);

	add_earth_button.set_text("Earth");
	add_earth_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_earth_button.set_size(left_width - 20, 30);
	add_earth_button.set_position(-left_width + 10, 90 - add_planet_page * window_height);

	add_mars_button.set_text("Mars");
	add_mars_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_mars_button.set_size(left_width - 20, 30);
	add_mars_button.set_position(-left_width + 10, 130 - add_planet_page * window_height);

	add_jupiter_button.set_text("Jupiter");
	add_jupiter_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_jupiter_button.set_size(left_width - 20, 30);
	add_jupiter_button.set_position(-left_width + 10, 170 - add_planet_page * window_height);

	add_saturn_button.set_text("Saturn");
	add_saturn_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_saturn_button.set_size(left_width - 20, 30);
	add_saturn_button.set_position(-left_width + 10, 210 - add_planet_page * window_height);

	add_uranus_button.set_text("Uranus");
	add_uranus_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_uranus_button.set_size(left_width - 20, 30);
	add_uranus_button.set_position(-left_width + 10, 250 - add_planet_page * window_height);

	add_neptune_button.set_text("Neptune");
	add_neptune_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_neptune_button.set_size(left_width - 20, 30);
	add_neptune_button.set_position(-left_width + 10, 290 - add_planet_page * window_height);

	add_planet_page_back_button.set_text("Back");
	add_planet_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_planet_page_back_button.set_size(left_width - 20, 30);
	add_planet_page_back_button.set_position(-left_width + 10, 440 - add_planet_page * window_height);



	count_dist_button.set_text("Distance");
	count_dist_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	count_dist_button.set_size(left_width - 20, 30);
	count_dist_button.set_position(-left_width + 10, 10 - count_page * window_height);

	count_angle_button.set_text("Angle");
	count_angle_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	count_angle_button.set_size(left_width - 20, 30);
	count_angle_button.set_position(-left_width + 10, 50 - count_page * window_height);

	count_page_back_button.set_text("Back");
	count_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	count_page_back_button.set_size(left_width - 20, 30);
	count_page_back_button.set_position(-left_width + 10, 440 - count_page * window_height);



	set_angle_button.set_text("Set angle");
	set_angle_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	set_angle_button.set_size(left_width - 20, 30);
	set_angle_button.set_position(-left_width + 10, 50 - obj_settings_page * window_height);

	add_config_button.set_text("Add config");
	add_config_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	add_config_button.set_size(left_width - 20, 30);
	add_config_button.set_position(-left_width + 10, 130 - obj_settings_page * window_height);

	obj_settings_page_back_button.set_text("Back");
	obj_settings_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	obj_settings_page_back_button.set_size(left_width - 20, 30);
	obj_settings_page_back_button.set_position(-left_width + 10, 440 - obj_settings_page * window_height);



	wq_button.set_text("Western square");
	wq_button.set_character_size(15);
	wq_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	wq_button.set_size(left_width - 20, 30);
	wq_button.set_position(-left_width + 10, 10 - config_page * window_height);

	eq_button.set_text("Eastern square");
	eq_button.set_character_size(15);
	eq_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	eq_button.set_size(left_width - 20, 30);
	eq_button.set_position(-left_width + 10, 50 - config_page * window_height);

	cs_button.set_text("Confrontation");
	cs_button.set_character_size(15);
	cs_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	cs_button.set_size(left_width - 20, 30);
	cs_button.set_position(-left_width + 10, 90 - config_page * window_height);

	cn_button.set_text("Connection");
	cn_button.set_character_size(15);
	cn_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	cn_button.set_size(left_width - 20, 30);
	cn_button.set_position(-left_width + 10, 130 - config_page * window_height);

	we_button.set_text("Western elongation");
	we_button.set_character_size(15);
	we_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	we_button.set_size(left_width - 20, 30);
	we_button.set_position(-left_width + 10, 170 - config_page * window_height);

	ee_button.set_text("Eastern elongation");
	ee_button.set_character_size(15);
	ee_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	ee_button.set_size(left_width - 20, 30);
	ee_button.set_position(-left_width + 10, 210 - config_page * window_height);

	uc_button.set_text("Upper connection");
	uc_button.set_character_size(15);
	uc_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	uc_button.set_size(left_width - 20, 30);
	uc_button.set_position(-left_width + 10, 250 - config_page * window_height);

	dc_button.set_text("Bottom connection");
	dc_button.set_character_size(15);
	dc_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	dc_button.set_size(left_width - 20, 30);
	dc_button.set_position(-left_width + 10, 290 - config_page * window_height);

	config_page_back_button.set_text("Back");
	config_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	config_page_back_button.set_size(left_width - 20, 30);
	config_page_back_button.set_position(-left_width + 10, 440 - config_page * window_height);



	easy_mode_button.set_text("Easy");
	easy_mode_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	easy_mode_button.set_size(left_width - 20, 30);
	easy_mode_button.set_position(-left_width + 10, 10 - practice_mode_page * window_height);

	normal_mode_button.set_text("Normal");
	normal_mode_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	normal_mode_button.set_size(left_width - 20, 30);
	normal_mode_button.set_position(-left_width + 10, 50 - practice_mode_page * window_height);

	hard_mode_button.set_text("Hard");
	hard_mode_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	hard_mode_button.set_size(left_width - 20, 30);
	hard_mode_button.set_position(-left_width + 10, 90 - practice_mode_page * window_height);

	statistics_button.set_text("Statistics");
	statistics_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	statistics_button.set_size(left_width - 20, 30);
	statistics_button.set_position(-left_width + 10, 130 - practice_mode_page * window_height);

	practice_mode_page_back_button.set_text("Back");
	practice_mode_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	practice_mode_page_back_button.set_size(left_width - 20, 30);
	practice_mode_page_back_button.set_position(-left_width + 10, 440 - practice_mode_page * window_height);



	submit_button.set_text("Submit");
	submit_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	submit_button.set_size(left_width - 60, 30);
	submit_button.set_position(-left_width + 10, 400 - task_page * window_height);

	next_button.set_text("Next");
	next_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	next_button.set_size(left_width - 70, 30);
	next_button.set_position(-40, 400 - task_page * window_height);

	task_page_back_button.set_text("Back");
	task_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	task_page_back_button.set_size(left_width - 20, 30);
	task_page_back_button.set_position(-left_width + 10, 440 - task_page * window_height);



	statistics_page_back_button.set_text("Back");
	statistics_page_back_button.set_color(Color(125, 174, 230), Color(25, 59, 72));
	statistics_page_back_button.set_size(left_width - 20, 30);
	statistics_page_back_button.set_position(-left_width + 10, 440 - statistics_page * window_height);



	background.setFillColor(Color(9, 39, 51));
	background.setSize(Vector2f(left_width + 60, window_height));
	background.setPosition(-left_width - 60, 0);

	data_background.setFillColor(Color(51, 122, 150));
	data_background.setSize(Vector2f(right_width, window_height));
	data_background.setPosition(window_width, 0);
}

int menu::check_mouse(RenderWindow & window)
{
	if (is_closed)
	{
		if (icon.is_clicked(window))
		{
			v = 10;
			is_closed = false;
			return -1;
		}
	}
	else
	{
		if (icon.is_clicked(window))
		{
			v = -10;
			is_closed = true;
			return -1;
		}



		if (practice_mode_button.is_clicked(window))
		{
			page = practice_mode_page;
			return -3;
		}
		if (sandbox_button.is_clicked(window))
		{
			page = sandbox_page;
			return -3;
		}


		if (add_system_button.is_clicked(window))
		{
			page = add_system_page;
			return -3;
		}
		if (add_star_button.is_clicked(window))
		{
			page = add_star_page;
			return -3;
		}
		if (add_planet_button.is_clicked(window))
		{
			page = add_planet_page;
			return -3;
		}
		if (count_smth_button.is_clicked(window))
		{
			page = count_page;
			return -3;
		}
		if (pause_button.is_clicked(window)) return 300;
		if (clear_button.is_clicked(window)) return 200;
		if (sandbox_page_back_button.is_clicked(window))
		{
			page = init_page;
			return 200;
		}


		if (add_solar_system_button.is_clicked(window)) return 40;
		if (add_system_page_back_button.is_clicked(window))
		{
			page = sandbox_page;
			return -3;
		}


		if (add_sun_button.is_clicked(window)) return 0;
		if (add_star_page_back_button.is_clicked(window))
		{
			page = sandbox_page;
			return -3;
		}


		if (add_mercury_button.is_clicked(window)) return 10;
		if (add_venus_button.is_clicked(window)) return 11;
		if (add_earth_button.is_clicked(window)) return 12;
		if (add_mars_button.is_clicked(window)) return 13;
		if (add_jupiter_button.is_clicked(window)) return 14;
		if (add_saturn_button.is_clicked(window)) return 15;
		if (add_uranus_button.is_clicked(window)) return 16;
		if (add_neptune_button.is_clicked(window)) return 17;
		if (add_planet_page_back_button.is_clicked(window))
		{
			page = sandbox_page;
			return -3;
		}


		if (count_dist_button.is_clicked(window)) return 50;
		if (count_angle_button.is_clicked(window)) return 51;
		if (count_page_back_button.is_clicked(window))
		{
			focused_obj.resize(0);
			data_type = -1;
			page = sandbox_page;
			return -3;
		}


		if (set_angle_button.is_clicked(window)) return 21;
		if (add_config_button.is_clicked(window))
		{
			page = config_page;
			return -3;
		}
		if (obj_settings_page_back_button.is_clicked(window))
		{
			focused_obj.resize(0);
			data_type = -1;
			page = sandbox_page;
			return -3;
		}


		if (wq_button.is_clicked(window)) return 30;
		if (eq_button.is_clicked(window)) return 31;
		if (cs_button.is_clicked(window)) return 32;
		if (cn_button.is_clicked(window)) return 33;
		if (we_button.is_clicked(window)) return 34;
		if (ee_button.is_clicked(window)) return 35;
		if (uc_button.is_clicked(window)) return 36;
		if (dc_button.is_clicked(window)) return 37;
		if (config_page_back_button.is_clicked(window))
		{
			page = obj_settings_page;
			return 104;
		}

		if (easy_mode_button.is_clicked(window))
		{
			settings[0] = 4;
			settings[1] = 5;
			settings[2] = 2;
			left_width += 60;
			submitted = false;
			page = task_page;
			return 60;
		}
		if (normal_mode_button.is_clicked(window))
		{
			settings[0] = 6;
			settings[1] = 7;
			settings[2] = 4;
			left_width += 60;
			submitted = false;
			page = task_page;
			return 60;
		}
		if (hard_mode_button.is_clicked(window))
		{
			settings[0] = 8;
			settings[1] = 9;
			settings[2] = 6;
			left_width += 60;
			submitted = false;
			page = task_page;
			return 60;
		}
		if (statistics_button.is_clicked(window))
		{
			left_width += 60;
			page = statistics_page;
			return -3;
		}
		/*if (practice_mode_page_back_button.is_clicked(window))
		{
			page = init_page;
			return 200;
		}*/

		int mouse_x = Mouse::getPosition(window).x;
		int mouse_y = Mouse::getPosition(window).y;
		if (page == task_page && 0 < mouse_x && mouse_x < cur_left_pos)
		{
			int rtn = 0;
			for (int question_num = 0; question_num < questions.size(); ++question_num)
			{
				if (10 + 45 * question_num <= mouse_y && mouse_y <= 10 + 45 * (question_num + 1)) return 70 + question_num;
			}
		}
		if (submit_button.is_clicked(window))
		{
			if (!submitted)
			{
				fstream file;
				file.open("data.txt", ios::in);
				vector <int> total(4);
				vector <int> answered(4);
				for (int i = 0; i < total.size(); ++i)
				{
					file >> total[i];
				}
				for (int i = 0; i < answered.size(); ++i)
				{
					file >> answered[i];
				}
				for (int question_num = 0; question_num < questions.size(); ++question_num)
				{
					if (question_type[question_num] == 2)
					{
						++total[0];
						if (fabs(user_ans[question_num] - correct_ans[question_num]) <= 0.01) ++answered[0];
					}
					else if (question_type[question_num] == 3)
					{
						++total[1];
						if (fabs(user_ans[question_num] - correct_ans[question_num]) <= 0.1) ++answered[1];
					}
					else if (question_type[question_num] == 4)
					{
						++total[2];
						if (fabs(user_ans[question_num] - correct_ans[question_num]) <= 0.1) ++answered[2];
					}
					else if (question_type[question_num] == 5)
					{
						++total[3];
						if (fabs(user_ans[question_num] - correct_ans[question_num]) <= 0.01) ++answered[3];
					}
				}
				file.close();
				file.open("data.txt", ios::out);
				for (auto& i : total)
				{
					file << i << " ";
				}
				file << "\n";
				for (auto& i : answered)
				{
					file << i << " ";
				}
				file << "\n";
				file.close();
				submitted = true;
			}
			return -3;
		}
		if (submitted && next_button.is_clicked(window))
		{
			submitted = false;
			return 60;
		}
		if (task_page_back_button.is_clicked(window))
		{
			left_width -= 60;
			page = practice_mode_page;
			return 200;
		}

		if (statistics_page_back_button.is_clicked(window))
		{
			left_width -= 60;
			page = practice_mode_page;
			return -3;
		}
	}

	return -2;
}

void menu::set_page(int new_page)
{
	page = new_page;
}

pair <int, int> menu::get_cur_pos()
{
	return { cur_left_pos, cur_right_pos };
}

void menu::move()
{
	cur_left_pos += v;
	cur_left_pos = max(0, cur_left_pos);
	cur_left_pos = min(left_width, cur_left_pos);
	cur_right_pos -= v * right_width / left_width;
	cur_right_pos = max(window_width - right_width, cur_right_pos);
	cur_right_pos = min(window_width, cur_right_pos);

	icon.set_position(cur_left_pos, 0);

	practice_mode_button.set_position (cur_left_pos - left_width + 10, 10 - init_page * window_height + page * window_height);
	sandbox_button.set_position       (cur_left_pos - left_width + 10, 50 - init_page * window_height + page * window_height);

	add_system_button.set_position        (cur_left_pos - left_width + 10, 10 - sandbox_page * window_height + page * window_height);
	add_star_button.set_position          (cur_left_pos - left_width + 10, 50 - sandbox_page * window_height + page * window_height);
	add_planet_button.set_position        (cur_left_pos - left_width + 10, 90 - sandbox_page * window_height + page * window_height);
	count_smth_button.set_position        (cur_left_pos - left_width + 10, 130 - sandbox_page * window_height + page * window_height);
	pause_button.set_position             (cur_left_pos - left_width + 10, 170 - sandbox_page * window_height + page * window_height);
	clear_button.set_position             (cur_left_pos - left_width + 10, 210 - sandbox_page * window_height + page * window_height);
	sandbox_page_back_button.set_position (cur_left_pos - left_width + 10, window_height - 40 - sandbox_page * window_height + page * window_height);

	
	add_solar_system_button.set_position     (cur_left_pos - left_width + 10, 10 - add_system_page * window_height + page * window_height);
	add_system_page_back_button.set_position (cur_left_pos - left_width + 10, window_height - 40 - add_system_page * window_height + page * window_height);

	
	add_sun_button.set_position             (cur_left_pos - left_width + 10, 10 - add_star_page * window_height + page * window_height);
	add_star_page_back_button.set_position  (cur_left_pos - left_width + 10, window_height - 40 - add_star_page * window_height + page * window_height);

	
	add_mercury_button.set_position          (cur_left_pos - left_width + 10, 10 - add_planet_page * window_height + page * window_height);
	add_venus_button.set_position            (cur_left_pos - left_width + 10, 50 - add_planet_page * window_height + page * window_height);
	add_earth_button.set_position            (cur_left_pos - left_width + 10, 90 - add_planet_page * window_height + page * window_height);
	add_mars_button.set_position             (cur_left_pos - left_width + 10, 130 - add_planet_page * window_height + page * window_height);
	add_jupiter_button.set_position          (cur_left_pos - left_width + 10, 170 - add_planet_page * window_height + page * window_height);
	add_saturn_button.set_position           (cur_left_pos - left_width + 10, 210 - add_planet_page * window_height + page * window_height);
	add_uranus_button.set_position           (cur_left_pos - left_width + 10, 250 - add_planet_page * window_height + page * window_height);
	add_neptune_button.set_position          (cur_left_pos - left_width + 10, 290 - add_planet_page * window_height + page * window_height);
	add_planet_page_back_button.set_position (cur_left_pos - left_width + 10, window_height - 40 - add_planet_page * window_height + page * window_height);


	set_angle_button.set_position              (cur_left_pos - left_width + 10, 50 - obj_settings_page * window_height + page * window_height);
	add_config_button.set_position             (cur_left_pos - left_width + 10, 130 - obj_settings_page * window_height + page * window_height);
	obj_settings_page_back_button.set_position (cur_left_pos - left_width + 10, window_height - 40 - obj_settings_page * window_height + page * window_height);


	wq_button.set_position               (cur_left_pos - left_width + 10, 10 - config_page * window_height + page * window_height);
	eq_button.set_position               (cur_left_pos - left_width + 10, 50 - config_page * window_height + page * window_height);
	cs_button.set_position               (cur_left_pos - left_width + 10, 90 - config_page * window_height + page * window_height);
	cn_button.set_position               (cur_left_pos - left_width + 10, 130 - config_page * window_height + page * window_height);
	we_button.set_position               (cur_left_pos - left_width + 10, 170 - config_page * window_height + page * window_height);
	ee_button.set_position               (cur_left_pos - left_width + 10, 210 - config_page * window_height + page * window_height);
	uc_button.set_position               (cur_left_pos - left_width + 10, 250 - config_page * window_height + page * window_height);
	dc_button.set_position               (cur_left_pos - left_width + 10, 290 - config_page * window_height + page * window_height);
	config_page_back_button.set_position (cur_left_pos - left_width + 10, window_height - 40 - config_page * window_height + page * window_height);

	
	count_dist_button.set_position      (cur_left_pos - left_width + 10, 10 - count_page * window_height + page * window_height);
	count_angle_button.set_position     (cur_left_pos - left_width + 10, 50 - count_page * window_height + page * window_height);
	count_page_back_button.set_position (cur_left_pos - left_width + 10, window_height - 40 - count_page * window_height + page * window_height);


	easy_mode_button.set_position               (cur_left_pos - left_width + 10, 10 - practice_mode_page * window_height + page * window_height);
	normal_mode_button.set_position             (cur_left_pos - left_width + 10, 50 - practice_mode_page * window_height + page * window_height);
	hard_mode_button.set_position               (cur_left_pos - left_width + 10, 90 - practice_mode_page * window_height + page * window_height);
	statistics_button.set_position              (cur_left_pos - left_width + 10, 130 - practice_mode_page * window_height + page * window_height);
	//practice_mode_page_back_button.set_position (cur_left_pos - left_width + 10, window_height - 40 - practice_mode_page * window_height + page * window_height);


	submit_button.set_position         (cur_left_pos - left_width + 10,  window_height - 80 - task_page * window_height + page * window_height);
	next_button.set_position           (cur_left_pos - left_width + 110, window_height - 80 - task_page * window_height + page * window_height);
	task_page_back_button.set_position (cur_left_pos - left_width + 10,  window_height - 40 - task_page * window_height + page * window_height);

	statistics_page_back_button.set_position (cur_left_pos - left_width + 10, window_height - 40 - statistics_page * window_height + page * window_height);


	if (page != task_page && page != statistics_page) background.setPosition(cur_left_pos - left_width - 60, 0);
	else background.setPosition(cur_left_pos - left_width, 0);
	data_background.setPosition(cur_right_pos, 0);
}

void menu::draw(RenderWindow& window)
{
	move();
	window.draw(background);
	window.draw(data_background);

	data = "";

	if (data_type == 0)
	{
		data += focused_obj[0]->get_name() + "\n\t";

		data += "Object data:\n\t\t";

		long double obj_mass = focused_obj[0]->get_mass();
		int mass_deg = 0;
		while (obj_mass > 1000)
		{
			++mass_deg;
			obj_mass /= 10.0;
		}
		int int_obj_mass = (int)obj_mass;
		data += "Mass = " + to_string(int_obj_mass / 100) + "." + to_string(int_obj_mass % 100) + "e" + to_string(mass_deg + 2) + " kg\n\t\t";

		long double obj_radius = focused_obj[0]->get_object_radius();
		int radius_deg = 0;
		while (obj_radius > 1000)
		{
			++radius_deg;
			obj_radius /= 10.0;
		}
		int int_obj_radius = (int)obj_radius;
		data += "Radius = " + to_string(int_obj_radius / 100) + "." + to_string(int_obj_radius % 100) + "e" + to_string(radius_deg + 2) + " km\n\t\t";

		long double obj_speed = focused_obj[0]->get_speed();
		int speed_deg = 0;
		while (obj_speed > 1000)
		{
			++speed_deg;
			obj_speed /= 10.0;
		}
		int int_obj_speed = (int)obj_speed;
		data += "Speed = " + to_string(int_obj_speed / 100) + "." + to_string(int_obj_speed % 100) + "e" + to_string(speed_deg + 2) + " km/s\n\t\t";

		long double orbit_period = focused_obj[0]->get_orbit_period();
		data += "Period = " + to_string(orbit_period) + " years\n\t\t";

		data += "Current distance to star = " + to_string(focused_obj[0]->get_dist_to_star()) + " ae\n\t";

		if (tasks.size() == 0)
		{
			data += "Configurations:\n\t\t";

			for (int i = 0; i < focused_obj[0]->connections.size(); ++i)
			{
				data += "is in ";
				if (focused_obj[0]->connections[i].second == "Western square") data += "Eastern elongation";
				if (focused_obj[0]->connections[i].second == "Eastern square") data += "Western elongation";
				if (focused_obj[0]->connections[i].second == "Confrontation") data += "Bottom connection";
				if (focused_obj[0]->connections[i].second == "Connection") data += "Upper connection";
				if (focused_obj[0]->connections[i].second == "Western elongation") data += "Eastern square";
				if (focused_obj[0]->connections[i].second == "Eastern elongation") data += "Western square";
				if (focused_obj[0]->connections[i].second == "Bottom connection") data += "Confrontation";
				if (focused_obj[0]->connections[i].second == "Upper connection") data += "Connection";
				data += " of " + focused_obj[0]->connections[i].first->get_name() + "\n\t\t";
			}
		}
	}
	else if (tasks.size() != 0)
	{
		data += "Place planets in this configurations:\n\t";
		for (auto& cur_task : tasks)
		{
			data += cur_task.second_obj->get_name() + " is in " + cur_task.config + " of " + cur_task.first_obj->get_name() + "\n\t";
		}
	}
	else
	{
		if (distances.size() == 0 && angles.size() == 0) data = "Choose a star or planet to get data";
		if (data_type == 1)
		{
			distances.push_back(focused_obj);
			data_type = -1;
		}
		else if (data_type == 2)
		{
			angles.push_back(focused_obj);
			data_type = -1;
		}

		if (distances.size() != 0) data += "Distance between:\n\t";
		for (auto& new_data : distances)
		{
			data += new_data[0]->get_name() + " and " + new_data[1]->get_name() + " = " + to_string(new_data[0]->get_dist_to_obj(new_data[1])) + " ae\n\t";
		}
		data += "\n";

		if (angles.size() != 0) data += "Angles:\n\t";
		for (auto& new_data : angles)
		{
			data += new_data[0]->get_name() + " - " + new_data[1]->get_name() + " - " + new_data[2]->get_name() + " = " + to_string(new_data[1]->get_3_obj_angle(new_data[0], new_data[2])) + " deg\n\t";
		}
		data += "\n";
	}

	Font font;
	Text text;

	font.loadFromFile("arial_narrow_7.ttf");
	text.setFont(font);
	text.setCharacterSize(15);
	text.setFillColor(Color(9, 39, 51));
	text.setString(data);
	text.setPosition(cur_right_pos + 10, 10);

	window.draw(text);



	icon.draw(window);

	if (page == init_page)
	{
		practice_mode_button.draw(window);
		sandbox_button.draw(window);
	}
	else if (page == sandbox_page)
	{
		add_system_button.draw(window);
		add_star_button.draw(window);
		add_planet_button.draw(window);
		count_smth_button.draw(window);
		pause_button.draw(window);
		clear_button.draw(window);
		sandbox_page_back_button.draw(window);
	}
	else if (page == add_system_page)
	{
		add_solar_system_button.draw(window);
		add_system_page_back_button.draw(window);
	}
	else if (page == add_star_page)
	{
		add_sun_button.draw(window);
		add_star_page_back_button.draw(window);
	}
	else if (page == add_planet_page)
	{
		add_mercury_button.draw(window);
		add_venus_button.draw(window);
		add_earth_button.draw(window);
		add_mars_button.draw(window);
		add_jupiter_button.draw(window);
		add_saturn_button.draw(window);
		add_uranus_button.draw(window);
		add_neptune_button.draw(window);
		add_planet_page_back_button.draw(window);
	}
	else if (page == count_page)
	{
		count_dist_button.draw(window);
		count_angle_button.draw(window);
		count_page_back_button.draw(window);
	}
	else if (page == obj_settings_page)
	{
		set_angle_button.draw(window);
		add_config_button.draw(window);
		obj_settings_page_back_button.draw(window);
	}
	else if (page == config_page)
	{
		wq_button.draw(window);
		eq_button.draw(window);
		cs_button.draw(window);
		cn_button.draw(window);
		we_button.draw(window);
		ee_button.draw(window);
		uc_button.draw(window);
		dc_button.draw(window);
		config_page_back_button.draw(window);
	}
	else if (page == practice_mode_page)
	{
		easy_mode_button.draw(window);
		normal_mode_button.draw(window);
		hard_mode_button.draw(window);
		statistics_button.draw(window);
		//practice_mode_page_back_button.draw(window);
	}
	else if (page == task_page)
	{
		text.setCharacterSize(15);
		for (int question_num = 0; question_num < questions.size(); ++question_num)
		{
			text.setFillColor(Color(125, 174, 230));
			text.setString(questions[question_num]);
			text.setPosition(cur_left_pos - left_width + 10, 10 + question_num * 45);
			window.draw(text);

			if (correct_ans[question_num] != -1)
			{
				if (submitted)
				{
					if ((question_type[question_num] == 1 && user_ans[question_num] == correct_ans[question_num]) || 
						(question_type[question_num] == 2 && fabs(user_ans[question_num] - correct_ans[question_num]) <= 0.01) ||
						(question_type[question_num] == 3 && fabs(user_ans[question_num] - correct_ans[question_num]) <= 0.1) ||
						(question_type[question_num] == 4 && fabs(user_ans[question_num] - correct_ans[question_num]) <= 0.1) ||
						(question_type[question_num] == 5 && fabs(user_ans[question_num] - correct_ans[question_num]) <= 0.01)) text.setFillColor(Color(0, 255, 0));
					else text.setFillColor(Color(255, 0, 0));
				}
				text.setString("\t" + to_string(user_ans[question_num]));
				text.setPosition(cur_left_pos - left_width + 10, 25 + question_num * 45);
				window.draw(text);

				if (submitted)
				{
					text.setFillColor(Color(255, 255, 255));
					text.setString("\tCorrect: " + to_string(correct_ans[question_num]));
					text.setPosition(cur_left_pos - left_width + 10, 40 + question_num * 45);
					window.draw(text);
				}
			}
		}

		submit_button.draw(window);
		if (submitted) next_button.draw(window);
		task_page_back_button.draw(window);
	}
	else if (page == statistics_page)
	{
		fstream file;
		file.open("data.txt", ios::in);
		vector <int> total(4);
		vector <int> answered(4);
		for (int i = 0; i < total.size(); ++i)
		{
			file >> total[i];
		}
		for (int i = 0; i < answered.size(); ++i)
		{
			file >> answered[i];
		}

		text.setCharacterSize(15);
		text.setFillColor(Color(125, 174, 230));

		text.setString("Number of correct answers\n\t\t\t\t\t/\n\tTotal questions asked");
		text.setPosition(cur_left_pos - left_width + 20, 10);
		window.draw(text);

		text.setString("In distances topic: " + to_string(answered[0]) + " / " + to_string(total[0]));
		text.setPosition(cur_left_pos - left_width + 5, 70);
		window.draw(text);

		text.setString("In angles topic: " + to_string(answered[1]) + " / " + to_string(total[1]));
		text.setPosition(cur_left_pos - left_width + 5, 85);
		window.draw(text);

		text.setString("In angular dimensions topic: " + to_string(answered[2]) + " / " + to_string(total[2]));
		text.setPosition(cur_left_pos - left_width + 5, 100);
		window.draw(text);

		text.setString("In phases topic: " + to_string(answered[3]) + " / " + to_string(total[3]));
		text.setPosition(cur_left_pos - left_width + 5, 115);
		window.draw(text);

		statistics_page_back_button.draw(window);
	}
}

vector<int> menu::get_settings()
{
	return settings;
}

void menu::reset_data()
{
	distances.resize(0);
	angles.resize(0);
	data_type = -1;
}

void menu::upd_questions(string _questions)
{
	questions.push_back(_questions);
}

void menu::upd_question_type(int _question_type)
{
	question_type.push_back(_question_type);
}

void menu::upd_user_ans(long double _user_ans, int ind)
{
	if (ind == -1) user_ans.push_back(_user_ans);
	else
	{
		while (ind >= user_ans.size())
		{
			user_ans.push_back(-1);
		}
		user_ans[ind] = _user_ans;
	}
}

void menu::upd_correct_ans(long double _correct_ans)
{
	correct_ans.push_back(_correct_ans);
}

void menu::clear_questions()
{
	questions.resize(0);
	question_type.resize(0);
	user_ans.resize(0);
	correct_ans.resize(0);
}
